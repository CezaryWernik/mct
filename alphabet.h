#ifndef _ALPHABET_H

#pragma region LETTERS

	#define A ".-"
	#define B "-..."
	#define C "-.-."
	#define D "-.."
	#define E "."
	#define F "..-."
	#define G "--."
	#define H "...."
	#define I ".."
	#define J ".---"
	#define K "-.-"
	#define L ".-.."
	#define M "--"
	#define N "-."
	#define O "---"
	#define P ".--."
	#define Q "--.-"
	#define R ".-."
	#define S "..."
	#define T "-"
	#define U "..-"
	#define V "...-"
	#define W ".--"
	#define X "-..-"
	#define Y "-.--"
	#define Z "--.."

#pragma endregion

#pragma region DIGITS

	#define _1 ".----"
	#define _2 "..---"
	#define _3 "...--"
	#define _4 "....-"
	#define _5 "....."
	#define _6 "-...."
	#define _7 "--..."
	#define _8 "---.."
	#define _9 "----."
	#define _0 "-----"

#pragma endregion

#pragma region SPECIAL

	#define DOT "_"

#pragma endregion

#endif